import os
from pathlib import Path
from typing import *

PathLike = Union[str, Path]

SUFFIX_PROCESSED = '_processed'
EXT_TRACK = 'wav'
EXT_FEATURES = 'mfcc'

# DN - Dir Name
# FN - File Name
# D - Dir (path)
# F - File (path)

DN_SAMPLES = 'samples'
DN_SAMPLES_PROCESSED = DN_SAMPLES + SUFFIX_PROCESSED
DN_FEATURES = 'features'
DN_FEATURES_PROCESSED = DN_FEATURES + SUFFIX_PROCESSED
DN_MODELS = 'models'

FN_HTK_CONFIG = 'HCopy.cnf'
FN_MODEL = 'model_epochs_{epoch:d}'

D_TMP = Path('tmp')
D_DATA = Path('data')
D_THIRD_PARTY = Path('third-party')
D_SAMPLES = D_DATA / DN_SAMPLES
D_SAMPLES_PROCESSED = D_DATA / DN_SAMPLES_PROCESSED
D_FEATURES = D_DATA / DN_FEATURES
D_FEATURES_PROCESSED = D_DATA / DN_FEATURES_PROCESSED
D_MODELS = D_TMP / DN_MODELS

FP_HTK_CONFIG = D_THIRD_PARTY / FN_HTK_CONFIG
FP_MODEL = D_MODELS / FN_MODEL

if not os.path.isdir(D_DATA):
    os.chdir('..')

for dirname in (
        D_TMP,
        D_DATA,
        D_THIRD_PARTY,
        D_SAMPLES,
        D_SAMPLES_PROCESSED,
        D_FEATURES,
        D_FEATURES_PROCESSED,
        D_MODELS,
):
    dirname.mkdir(exist_ok=True)