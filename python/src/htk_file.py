import struct

from src.consts import *

__all__ = ['HTKFile']


class HTKFile:
    """ Class to load binary HTK file.
        Details on the format can be found online in HTK Book chapter 5.7.1.
        Not everything is implemented 100%, but most features should be supported.
        Not implemented:
            CRC checking - files can have CRC, but it won't be checked for correctness
            VQ - Vector features are not implemented.
    """

    data = None
    nSamples: int = 0
    nFeatures: int = 0
    sampPeriod: int = 0
    basicKind = None
    qualifiers = None
    endian: str = '>'

    def __init__(self, filename: PathLike):
        self.load(filename)

    def load(self, filename: PathLike):
        """ Loads HTK file.
            After loading the file you can check the following members:
                data (matrix) - data contained in the file
                nSamples (int) - number of frames in the file
                nFeatures (int) - number if features per frame
                sampPeriod (int) - sample period in 100ns units (e.g. fs=16 kHz -> 625)
                basicKind (string) - basic feature kind saved in the file
                qualifiers (string) - feature options present in the file
        """
        with open(filename, "rb") as f:

            header = f.read(12)
            self.nSamples, self.sampPeriod, sampSize, paramKind = struct.unpack(">iihh", header)
            if self.nSamples < 0 or self.sampPeriod < 0 or sampSize < 0:
                self.endian = '<'
                self.nSamples, self.sampPeriod, sampSize, paramKind = struct.unpack(self.endian + "iihh", header)
            basicParameter = paramKind & 0x3F

            self.basicKind = {
                0: "WAVEFORM",
                1: "LPC",
                2: "LPREFC",
                3: "LPCEPSTRA",
                4: "LPDELCEP",
                5: "IREFC",
                6: "MFCC",
                7: "FBANK",
                8: "MELSPEC",
                9: "USER",
                10: "DISCRETE",
                11: "PLP"
            }.get(basicParameter, 'ERROR')

            self.qualifiers = []
            if (paramKind & 0o100) != 0: self.qualifiers.append("E")
            if (paramKind & 0o200) != 0: self.qualifiers.append("N")
            if (paramKind & 0o400) != 0: self.qualifiers.append("D")
            if (paramKind & 0o1000) != 0: self.qualifiers.append("A")
            if (paramKind & 0o2000) != 0: self.qualifiers.append("C")
            if (paramKind & 0o4000) != 0: self.qualifiers.append("Z")
            if (paramKind & 0o10000) != 0: self.qualifiers.append("K")
            if (paramKind & 0o20000) != 0: self.qualifiers.append("0")
            if (paramKind & 0o40000) != 0: self.qualifiers.append("V")
            if (paramKind & 0o100000) != 0: self.qualifiers.append("T")

            self.nFeatures = sampSize // 2 if "C" in self.qualifiers or "V" in self.qualifiers or self.basicKind is "IREFC" or self.basicKind is "WAVEFORM" else sampSize // 4

            if "C" in self.qualifiers: self.nSamples -= 4

            if "V" in self.qualifiers: raise NotImplementedError("VQ is not implemented")

            self.data = []
            if self.basicKind is "IREFC" or self.basicKind is "WAVEFORM":
                for x in range(self.nSamples):
                    s = f.read(sampSize)
                    frame = []
                    for v in range(self.nFeatures):
                        val = struct.unpack_from(self.endian + "h", s, v * 2)[0] / 32767.0
                        frame.append(val)
                    self.data.append(frame)
            elif "C" in self.qualifiers:
                A = []
                s = f.read(self.nFeatures * 4)
                for x in range(self.nFeatures):
                    A.append(struct.unpack_from(self.endian + "f", s, x * 4)[0])
                B = []
                s = f.read(self.nFeatures * 4)
                for x in range(self.nFeatures):
                    B.append(struct.unpack_from(self.endian + "f", s, x * 4)[0])

                for x in range(self.nSamples):
                    s = f.read(sampSize)
                    frame = []
                    for v in range(self.nFeatures):
                        frame.append((struct.unpack_from(self.endian + "h", s, v * 2)[0] + B[v]) / A[v])
                    self.data.append(frame)
            else:
                for x in range(self.nSamples):
                    s = f.read(sampSize)
                    frame = []
                    for v in range(self.nFeatures):
                        val = struct.unpack_from(self.endian + "f", s, v * 4)
                        frame.append(val[0])
                    self.data.append(frame)

            # if "K" in self.qualifiers:
            # print("CRC checking not implememnted...")
