DIRDATA = data
DIRSAMPLES = $(DIRDATA)/samples
DIRFEATURES = $(DIRDATA)/features
DIRSCRIPTS = scripts
PREPROCCESED_SUFFIX = _processed

HTK_CONFIG = third-party/HCopy.cnf

BACKEND_CPU = CPU
BACKEND_MKL = MKL
BACKEND_NVCC = NVCC

ifndef BACKEND
	BACKEND=CPU
endif

export BACKEND

all: reverb features

clean-make: clean all

unpackdata:
ifeq (,$(wildcard ./$(DIRSAMPLES)/*.wav))
	bash $(DIRSCRIPTS)/extraction.sh $(DIRDATA) $(DIRSAMPLES)
endif

reverb: unpackdata
	bash $(DIRSCRIPTS)/reverb.sh $(DIRSAMPLES) $(PREPROCCESED_SUFFIX)

htk:
	cd third-party && make all

features: reverb htk
	bash $(DIRSCRIPTS)/features.sh $(DIRFEATURES) $(HTK_CONFIG) ${DIRDATA} $(BACKEND)

clean: clean-reverbs clean-features clean-tmp

clean-reverbs:
	rm -rfv $(DIRSAMPLES)$(PREPROCCESED_SUFFIX)

clean-features:
	rm -rfv $(DIRFEATURES) $(DIRFEATURES)$(PREPROCCESED_SUFFIX)

clean-samples:
	rm -rfv $(DIRSAMPLES)

clean-tmp:
	# find -name '*.npy' | parallel rm -rfv {}
	rm -rfv tmp
