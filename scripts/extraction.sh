#!/bin/bash
DIRDATA=${1}
DIRSAMPLES=${2}

if [[ ! -d ${DIRSAMPLES} ]]; then
    7z x ${DIRDATA}/data.7z -o${DIRSAMPLES}
fi
