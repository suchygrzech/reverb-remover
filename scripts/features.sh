#!/bin/bash
DIRFEATURES=${1}
HTK_CONFIG=${2}
DIRDATA=${3}
BACKEND=${4}

mkdir -p ${DIRFEATURES}

BACKEND=${BACKEND,,}
if [[ ${BACKEND} == '' ]]; then
    binpath=$(find -type d -name '*bin.*')
    BACKEND=${binpath##*.}
fi

function generateFeatures() {
    baseFilepath=${1}
    configPath=${2}
    backend=${3}

    filepath="${baseFilepath/samples/features}"
    filepath="${filepath/wav/mfcc}"

    if [[ ! -f ${filepath} ]]; then
        mkdir -p ${filepath%/*}
        echo "${baseFilepath} -> generating features -> ${filepath}"
        ./third-party/htk/bin.${backend}/HCopy -C ${configPath} ${baseFilepath} ${filepath}
    fi
}

export -f generateFeatures
parallel generateFeatures {} ${HTK_CONFIG} ${BACKEND} -j $(nproc) ::: $(find ${DIRDATA} -name '*.wav')
