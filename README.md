# reverb-remover

Autoencoder removing reverb from audio signal using MFCC audio features

# Wymagania

Program wymaga co najmniej **Pythona3.6** oraz paczek wylistowanych w pliku `requirements.txt`.
By je zainstalować wymagany jest menadżer paczek `pip`.

```
> python -m pip install -U -r requirements.txt
```

# Instrukcja

## Dane

W git-lfs znajduje się archiwum `*.7z` ze ścieżkami w formacie `*.wav`.
By pobrać dane z git-lfs:
```
> git-lfs install && git-lfs fetch
```

Pliki dane do analizy należy umieścić w katalogu `data/samples`.

## Makefile

`makefile` zawiera kilka pomocniczych komend:
    -  `reverb` dodaje do każdego pliku pogłos za pomocą programu `sox` (musi być widoczny w systemie z linii komend)  
    -  `htk` buduje program HTK do generacji cech mfcc dla plików audio  
    -  `features` generuje za pomocą `HCopy` pliki `*.mfcc` zawierające wektory cech dla odpowiadających plików audio  
    -  `clean` czyści przetworzone pliki (wynik `reverb`), cechy (wynik `features`), modele (katalog `tmp`)   
    -  `clean-reverb` usuwa przetworzone pliki `*.wav`  
    -  `clean-features` usuwa pliki `*.mfcc`  
    -  `clean-samples` usuwa pliki `*.wav`  
    -  `clean-tmp` usuwa katalog `tmp`  

## Uczenie

By uruchomić uczenie modelu, wystarczy wywołać skrypt `run.py` w katalogu `python`.

```
> python run.py
```

# Docker

Testy i cały proces implementacji przebiegały w kontenerze dockera.
By zbudować obraz, uruchom z katalogu głównego:

```
> docker build -f docker/Dockerfile -t revrm:latest .
```

**Uwaga!!! Kontener oczekuje zamontowanego katalogu głównego w `/revrm` .**

Uruchomienie kontenera ze wsparciem GPU:

```
> nvidia-docker run --rm -ti -P 6006:6006 -v $(pwd):/revrm revrm:latest 
```
lub
```
> docker run --rm -ti -P 6006:6006 --runtime=nvidia -v $(pwd):/revrm revrm:latest
```
Potrzebne jest mapowanie portu `6006`, by móc uruchomić z kontenera `tensorboard` i móc obserwować przebieg uczenia.

```
> docker exec -ti [id kontenera] bash
> tensorboard --logdir logs
```

Informacje logowane w Tensorboard to:
 - `ratio` - określa stosunek:  
    $` \frac{d_1}{d_2} `$, gdzie
    $` d_1 = |OR| `$, a $` d_2 = |OP| `$.  
    $` O `$ - wektor **o**ryginalny, nieprzetworzony;  
    $` P `$ - wektor **p**rzewidziany z kontekstu przez model;  
    $` R `$ - wektor z pogłosem (**r**everb);  
    - `ratio_max` - maksymalna wartość stosunku odległości w zbiorze walidacyjnym
    - `ratio_min` - minimalna wartość stosunku odległości w zbiorze walidacyjnym
    - `ratio_avg` - średnia wartość stosunku odległość w zbiorze walidacyjnym
  - `train loss` - wartość funkcji straty dla zbioru próbek zbioru treningowego
  - `validation loss` - wartość funkcji straty dla zbioru walidacyjnego

# Wyniki

Zostały przetestowane różne konfiguracji sieci (różna liczba warstw i neuronów) oraz różne sposoby i szybkości uczenia.

Najlepsza dotychczas znaleziona konfiguracja przedstaiona jest poniżej:

![Konfiguracja sieci](readme_res/config.png)

Materiały dotyczące rozważanego zadania z wykorzystaniem autoenkodera nie zostały znalezione, dlatego przedstawiona architektura to przykładowy autoenkoder z artykułu internetowego dot. autoenkoderów.

Wartości metryk po 71 epochach uczenia przedstawiają się następująco:

![Train loss](readme_res/train_loss.png)

![Validation loss](readme_res/validation_loss.png)

![Ratio](readme_res/ratio.png)

Na wykresie z `ratio` widzimy 3 linie, które kolejno (od dołu) oznaczają `ratio_min`, `ratio_avg` oraz `ratio_max`.

Na wszystkich wykresach użyto dość wysokiej wartości wygładzania, by zobaczyć trendy przy dość "poszarpanych", widocznych w tle, przebiegach. 
Widać mimo wszystko, że sieć się uczy i polepsza nieznacznie swoje predykcje. 
Ze względu na ograniczony czas i długi czas uczenia nie zostało zbadanych wiele konfiguracji sieci.

Funkcją straty w przypadku rozważanego zadania jest odległość euklidesowa wektorów w 39-wymiarowej przestrzeni cech.

Na razie została zimplementowana sieć w pełni połączona, jednak warto przemyśleć użycie sieci rekurencyjnej, ze względu na sekwencyjny charakter danych.
Niedługo zostanie zaimplementowana obsługa takich sieci.

